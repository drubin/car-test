// ENV var for docker
var constants = require('./constants.js');
var mongoose = require("mongoose");


if(process.env.MONGO_URI){
  //Ideally this should come from a param but we are running in Docker so safe assumption
  mongoose.connect(process.env.MONGO_URI);
}else{
  //Ideally this should come from a param but we are running in Docker so safe assumption
  mongoose.connect("mongodb://mongo:27017");
}

var CarSchema = mongoose.Schema({
    name:  { type: String, required: true, unique: true },
    x: Number,
    y: Number,
    active: Boolean,
    desiredX: Number,
    desiredY: Number,
});

CarSchema.methods.speak = function () {
  var greeting = this.name
    ? "Meow name is " + this.name
    : "I don't have a name";
  console.log(greeting);
}

var Car = mongoose.model('Car', CarSchema);

function getCarMap(callback){
  var carMap = new Array(constants.MAP_SIZE);
  for (var i = 0; i < constants.MAP_SIZE; i++) {
    carMap[i] = new Array(constants.MAP_SIZE);
  }
  Car.find({}, function(err, cars) {
    cars.forEach(function(car) {
      //Do handle multiple cars on a single co-ord
      if(!carMap[car.x][car.y]){
        carMap[car.x][car.y] = car.name;
      }else{
        carMap[car.x][car.y] = carMap[car.x][car.y] + "," +car.name;
      }
    });
    callback(carMap);
  });

}

exports.Car = Car;
exports.getCarMap = getCarMap;
