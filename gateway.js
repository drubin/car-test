//# create an app.js file with the following contents
var constants = require('./constants.js');
var models = require('./data-models.js');
var express = require('express');
var app = express();

var Car = models.Car;

app.post('/api/move/:carId/:x/:y', function(req, res){

  if(req.params.x >= constants.MAP_SIZE || req.params.x < 0){
    res.send("Failed: Please use value with in map bounds 0 < "+ constants.MAP_SIZE +" > 20" );
    return;
  }
  if(req.params.y >= constants.MAP_SIZE || req.params.y < 0){
    res.send("Failed: Please use value with in map bounds 0 < "+ constants.MAP_SIZE +" > 20" );
    return;
  }

  Car.findById(req.params.carId, function(err, car){
    // No value bounding checking
    car.desiredX = req.params.x;
    car.desiredY = req.params.y;
    car.active = true;
    car.save().then(function(doc){
      console.log(car);
      res.send("OK");
    });
  });

});


app.listen(3000, function(){
  console.log('Example app listening on port 3000!');
});
