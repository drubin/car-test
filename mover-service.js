//DB setup
var constants = require('./constants.js')
var mongoose = require("mongoose");
var redis = require('./redis.js');
var functions = require('./functions.js');


var sub = redis.sub;
var pub = redis.pub;

var models = require('./data-models.js');
Car = models.Car;




// Update cars runs ever 10 seconds if a car is not active it sets its desired position to some where on the map
function updateCars(){
  console.log("Looking for inactive cars");

  Car.find({active: false}, function(err, cars) {
    console.log("Found "+cars.length + "inactive Cars");
    cars.forEach(function(car) {
      // Move car randomly around the map
      car.desiredX = Math.floor(Math.random()*constants.MAP_SIZE);
      car.desiredY = Math.floor(Math.random()*constants.MAP_SIZE);
      console.log("Setting "+ car.name + "to move to ["+ car.desiredX +","+car.desiredY+"]");
      car.active=true;
      car.save().then(function(doc){});
    });
  });
}


function moveCar(){
  Car.find({active:true, desiredY: { $gt: -1 }, desiredX: { $gt: -1 }}, function(err, cars) {
    console.log("Looking to move "+cars.length + " Cars");
    cars.forEach(function(car) {
      console.log(car.x, car.desiredX, car.y, car.desiredY);
      var moveMap = functions.nextPoint(car.x, car.desiredX, car.y, car.desiredY);
      console.log(car);
      console.log(moveMap);
      car.x = moveMap.x
      car.y = moveMap.y;

      if (car.x == car.desiredX && car.y == car.desiredY){
        car.desiredX = -1;
        car.desiredY = -1;
        car.active = false;
      }
      car.save().then(function(doc){
        console.log("saveing car", doc);
        pub.publish("car-update", "update");
      });
    });
  });
}
// Should slow move the car over 10 seconds.
// For now we are just going to move them 1 step every second.
setInterval(moveCar, 1000)


// One every 30seconds
setInterval(updateCars,30000)


