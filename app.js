//# create an app.js file with the following contents
var constants = require('./constants.js');
var models = require('./data-models.js');
var redis = require('./redis.js');
var express = require('express');
var app = express();
var engine = require('ejs-locals');
app.set('template_engine', 'ejs');
app.engine('ejs', engine);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static('public'))

var expressWs = require('express-ws')(app);

var sub = redis.sub;
var pub = redis.pub;

Car = models.Car;



app.get('/', function(req, res){
  Car.find({}, function(err, cars) {
    res.render('index',{cars:cars});
  })
});

app.get('/map', function(req, res){
  var carMap = new Array(constants.MAP_SIZE);
  for (var i = 0; i < constants.MAP_SIZE; i++) {
    carMap[i] = new Array(constants.MAP_SIZE);
  }
  Car.find({}, function(err, cars) {
    cars.forEach(function(car) {
      //Do handle multiple cars on a single co-ord
      carMap[car.x][car.y] = car.name;
    });
    res.render('map',{carMap:JSON.stringify(carMap)});
  });
});





app.get('/fill-db', function(req, res){
  for(var i = 0; i < 10; i++){
    var car = new Car({ name: 'Car '+i, x: i, y: i, desiredX: -1, desiredY: -1, active: false });
    car.save().then(function (doc) {
      console.log('Saving '+ doc.name);
    });
  }
  res.send("Filled Car DB");
});

app.get('/cars', function(req, res){
  var MAP_SIZE = 20;
  var carMap = new Array(constants.MAP_SIZE);
  for (var i = 0; i < constants.MAP_SIZE; i++) {
    carMap[i] = new Array(constants.MAP_SIZE);
  }
  Car.find({}, function(err, cars) {
    cars.forEach(function(car) {
      //Do handle multiple cars on a single co-ord
      console.log(car.x+ "  "+ car.y + "  "+car.name);
      carMap[car.x][car.y] = car.name;
    });
    var data = "";
    for(var x = 0 ; x < constants.MAP_SIZE ; x++){
      for(var y = 0 ; y < constants.MAP_SIZE ; y++){
          if(carMap[x][y]){
            data += "," + carMap[x][y];
          }
          else {
            data +="\t";
          }
      }
      data +="\n";
    }
    res.send(data)
  });
});

app.get('/send', function(req, res){
  pub.publish("a nice channel", "I am sending a message.");
  res.send("Sent message");
});






var router = express.Router();

router.ws('/echo', function(ws, req) {
  console.log("in here");
  ws.on('message', function(msg) {
    console.log("Got message" + msg);
    //ws.send(msg);
  });
  // Once the ws is connected subscribe to changes on our internal message queue
  sub.on("message", function (channel, message) {
    console.log("sub channel " + channel + ": " + message);
    // Should be a promise.
    models.getCarMap(function(mapData){
      ws.send(JSON.stringify({carMap: mapData}));
    })
  });
});

app.use("/ws-stuff", router);


app.listen(3000, function(){
  console.log('Example app listening on port 3000!');

});


var msg_count = 0;

sub.on("subscribe", function (channel, count) {
  console.log("Subscribing to "+ channel);
});

sub.subscribe("car-update");

// sub.on("message", function (channel, message) {
//   console.log("Static" + channel + ": " + message);
// });
