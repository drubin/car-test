# Test app

### Setup instructions 

  npm install # seems to be needed for shared resources between docker containers
  docker-compose up


## Services 
  
This will start up 5 different services 

  * Redis 
  * MonogoDB 
  * A web front end intended to be view in a browser.  (port 3000)
  * A background task processing queue a "mover-service"
  * A API gateway fro controlling the system to be used via Curl  (port 3001)


# How to run the system locally. 

1. First get the docker compose setup working
1. This should start all the required services and they should be linked together with docker's hostname injection
1. Next fill your database will cars. Browse to http://localhost:3000/fill-db
1. You can then view all your a rough dump of all the data http://localhost:3000/
1. You can view a visual repersentation of the cars positions at http://localhost:3000/map 
1. The background task is supposed to run every 30 seconds find a car that is inactive and move them to a random position on the map


# API access

You can make curl requests to the api. Note you will require the car's ID from the car list page

  curl -d "" http://localhost:3001/api/move/_id_/_x_/_y_

So an example would be to move car 58a8782dc2183c0022349f1a to the bottom right position in the map

  curl -d "" http://localhost:3001/api/move/58a8782dc2183c0022349f1a/19/19



# Some Assumptions/information

1. There seems to be a race condition with the Websockets and the redispublish subscribe model which can cause a crash of the web front end (restarting the web process will fix this) This is less than ideal and I would have liked to fix it
1. Security has mostly been ignored. Publish channels, no access control or authentication
1. The node code should be refactored to 
  1. Use objects/models better
  1. Use promises instead of callbacks (Honestly I didn't have enough time to read up on the promise changes since I last used node)


# Pictures 

  ![Map](https://www.dropbox.com/s/n26fme0pkt4cnmj/Screenshot%202017-02-18%2019.29.50.png?dl=0)
  ![Anmiation](https://www.dropbox.com/s/unow6syd4qhgbgl/anmiation.gif?dl=0)

