exports.nextPoint = function(x1, x2, y1, y2){
  var x = x1;

  if(x1 > x2){
    x = x1 - 1;
  }else if(x1 < x2) {
    x = x1 +1;
  }

  var y = y1;
  if(y1 > y2){
    y = y1 - 1;
  }else if(y1 < y2) {
    y = y1 +1;
  }


  return {x: x, y: y};
}
