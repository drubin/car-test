//DB setup
var redis = require("redis");

var sub;
var pub;

if(process.env.REDIS_PORT && process.env.REDIS_HOST){
  sub = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
  pub = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
}else{
  sub = redis.createClient('6379', 'redis');
  pub = redis.createClient('6379', 'redis');
}


exports.sub = sub;
exports.pub = pub;
