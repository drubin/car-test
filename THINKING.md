# My thoughts 
A raw dump of my throughts please excuse spelling/* but I thought it was useful to share these as a log mostly unedited.

1. Given amount of services/systems corners will have to be cut, simpliest MVP of each service which could be extended after.
1. A full map based system is too complex to build in a given time. Limit to simple x/y cords with a 2 D array (easily extended to proper geo cords)
1. 2D array is much easier to generate and display and show a working system of what they have requested with out assuming "map" == geo map
1. Prefer cattle over peds. Setting up auto scalling mongo/message queues is more complex and I am considering it out of time constraints 
1. Assume all code is cattle assume all data services is pets (given time constraints)
1. Design mongo db as a reltional db instead of a raw document store because I have experience doing that
1. Must remember node runs in callback async mode. 
1. Ignore security explitly in this test until last stage . Open websockets / apis are bad
1. Even if i did pick a long/lat system roads/water would be a massive issue..
1. Find a routing algorithm. 
  1. Found Lee's algorithm 
  1. https://en.wikipedia.org/wiki/A*_search_algorithm 
  1. Seem overly complex and I might mess them up
